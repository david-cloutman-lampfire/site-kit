/**
 * Declare globals.
 */

/**
 * Easy to type aliases for DOM query functions.
 *
 * Vanilla JavaScript can be difficult to type. The _d variable is a shim that
 * minimizes typos and makes using JS selector methods easy to use.
 */
var _d = (function (window, document) {
	// Attach selector methods to _d.
    console.log(document);
	return {
        all: document.querySelectorAll.bind(document),
        first: document.querySelector.bind(document),
        byId:  document.getElementById.bind(document),
        byClass:  document.getElementsByClassName.bind(document),
        byTag: document.getElementsByTagName.bind(document)
	};
})(window, document);



/**
 * Define core logic for application.
 */
var _app = (function (document, window) {
    // Declare private variables.
    let foo = true;


    // Return the public interface.
    return {
        /**
         * The main entry point for the application.
         */
        run: function () {},

        /**
         * A sample getter.
         */
        getFoo: function () {
            return foo;
        },

        /**
         * A sample setter.
         */
        setFoo: function (v) {
            foo = v;
        }
    };
})(document, window);



// Kick off the execution of _app. Do not modify past this comment.
(function (document, window) {
    _app.run();
})(document, window);
